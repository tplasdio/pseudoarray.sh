# Pseudoarray.sh

A POSIX shell library that implements array emulation. A pseudo array is just
a regular shell variable, but formatted in a way that can be restored unto the
native POSIX array `$@`.

## Usage

```sh
#!/bin/sh
. ./rest
. ./arr
. ./array_push

@ A = 1 2 3           # Create pseudoarray $A / Reestablish its elements
@ A += 4 5            # Push elements to the pseudoarray
@ A *= 2              # Duplicate the pseudoarray

eval "set -- $A"      # Restore the elements of $A unto $@ (i.e "${1}" "${2}", etc.)
for a do              # Iterate over the elements of $@
  <commands>
done

@ B = "$@"            # Save all elements from $@ to $B
@ C = "${3}" "${1}"   # Save some elements from $@ to $C

A="${B%?}${C}"        # Concatenate 2 pseudoarrays unto another
A=$(rest "$B" "$C")   # Create multidimensional pseudoarray
```

I also include a `readhd` function that reads here-documents and saves it to a
variable `$REPLY` by default, and a command `@end` that evaluates REPLY.
The benefit is that the commands are not executed in a subshell and can modify
`$@` of the caller. One can use it like this:

```sh
#!/bin/sh
. ./readhd
. ./evalhd
readhd <<EOF
<commands>
EOF
@end
```

With this, there's also another way to restore elements from `$A` unto `$@`,
so you don't have to type `eval`:

```sh
@set "$A"; @end
```

## To-do
 - [ ] Integrate and document the rest of the scripts
 - [ ] Add and improve functionality to '@'
