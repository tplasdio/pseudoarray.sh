#!/bin/sh
# Uso:
# @ A = "el1" "el2"
# @ A += "el3" "el4"
# @ A *= 3
# Bug:
# - Adjuntar seudoarreglos a otro seudoarreglo no da lo que se esperaría, o sea
#	@ A += "$B" "$C" ;no da igual a: arr A = "$B" "$C". Nota: ahora que veo el
#	comportamiento de Python, creo que debería simplemente adjuntarse ambos seudoarreglos
#	en el otro, no crear un seudoarreglo multidimensional
# Notas:
# - Para adjuntar seudoarreglos siempre puedes hacer simplemente:
#	A="${B%?}""$C"
#	Para que un seudoarreglo sea igual a otro, solo asignalos, son variables simplemente:
#	A="$B"
#	Para crear arreglos multidimensionales tendrías que hacer:
#	A=$(rest "$B" "$C")
#	Y el restablecerlo:
#	eval "set -- $A"
#	Se pondrían 2 seudoarreglos en $1 y $2, luego podrías hacer:
#	eval "set -- $1"
#	Podrías encontrarle utilidad en bucles anidados, dentro de subshells
# TODO:
# - Agregar una forma sencilla para adjuntar seudoarreglos, para discernir entre
#	seudoarreglos y elementos regulares, se me ocurre quizá prefijarlos con @ como Perl:
#	@ A += @B @C "elemento1" "elemento 2"
#	y cuando alguien quiera poner un elemento que empiece con '@', que lo prefije con otro @: @@
# - Mantener una lista (un seudoarreglo) de las variables que son seudoarreglos
# - Indexación, por ejemplo:
#	@ A[3]
#	@ A[-1]
#	@ A[3] = 4
#	@ A = @B[3] @C[0] @B[5]
# - Rebanada, por ejemplo:
#	@ A[0:4]
#	@ A[-4:-2]
#	@ A[0:4] = 4
#	@ A = @B[1:2] @C[3:-1]

_Pa_array(){
	case "${1-}" in
		'' | _ | [0-9]* | *[!0-9A-Za-z_]*) return 2 ;;
	esac
	case "${2-}" in
		=)
			_array="$1"
			shift 2
			eval "$_array"'="$(rest "$@")"' && \unset -v _array
		;;
		+=)
			_array="$1"
			shift 2
			_Pa_array_push "$_array" "$@" && return $?
		;;
		*=)
			case "${3#+}" in
				*[!0-9]* | '') return 3 ;;
				1 ) return ;;
				0 ) \unset -v "$1" && return ;;
			esac
			_array="$1"
			_prod="$3"
			# This is probably suboptimal and slow
			eval "set -- $(eval echo \"\$$_array\")"
			eval "set -- $(awk 'BEGIN{while(i++<'"$_prod"')printf"\"$@\" "}')"
			eval "$_array"'="$(rest "$@")"' && \unset -v _array _prod
		;;
		*) return 1 ;;
	esac
}

alias @='_Pa_array'
